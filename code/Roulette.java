import java.util.Scanner;
public class Roulette{
    public static void main(String[] args){
        boolean playAgain = true;
        double totalMoney = 1000;
        while(playAgain){
            RouletteWheel wheel = new RouletteWheel(); 
            int choice;
            double betAmount;
            Scanner scan = new Scanner(System.in);
            System.out.println("Welcome to Roulette!");
            System.out.println("How much would you like to bet for this round?");
            betAmount = scan.nextInt();
            totalMoney -= betAmount;
            System.out.println("Please enter a number from 0-37 as your guess");
            choice = scan.nextInt();
            wheel.spin();
            System.out.println("Spinning");
            System.out.println("Spinning");
            System.out.println("Spinning");
            System.out.println("Spinning");
            System.out.println("Landing on..." + wheel.getValue());

            if(choice == wheel.getValue()){
                totalMoney = (betAmount * 35) + totalMoney;
                System.out.println("Congrats! You got it right!");
                System.out.println("Your new total is : " + totalMoney + "$"); 
            }
            else{
                System.out.println("Uh Oh! You lost :( ");
                System.out.println("Your new total is : " + totalMoney + "$");
                System.out.println("You should have listened to your mom and not gambled!");
            }
            int replay;
            System.out.println("Would you like to play again? (1 = yes, 0 = no)");
            replay = scan.nextInt();
            if(replay == 0){
                playAgain = false;
            }
        }
        System.out.println("Thanks for playing!");
    }
}