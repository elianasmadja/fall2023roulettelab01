import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int number = 0;

    public RouletteWheel(){
        this.random = new Random();
    }
    public void spin(){
        this.number = random.nextInt(37);
    }
    public int getValue(){
        return this.number;
    }
}
